import MyButton from "@/components/UI/MyButton"
import MyInput from "@/components/UI/MyInput"
import MyMaskedNumberInput from "@/components/masked/MyMaskedNumberInput"
import MyMaskedNameInput from "@/components/masked/MyMaskedNameInput"
import MyMaskedEmailInput from "@/components/masked/MyMaskedEmailInput"
import MyCheckbox from "@/components/UI/MyCheckbox"
import CustomSelect from "@/components/UI/CustomSelect"

export default [
MyButton,
MyInput,
MyMaskedNumberInput,
MyCheckbox,
MyMaskedNameInput,
MyMaskedEmailInput,
CustomSelect
]